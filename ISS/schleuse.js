/*
    Code for X3D-Script-Node "statemachine_SCRIPT" in file ISS_X3D_Schleuse.x3d
    Author: Carl Kabitz
*/

function initialize () {
  // True == no pressure, False == normal pressure
  pressureIndoors = false;

  // indicating closing state of outer door
  outsideOpen = false;

  // indicating closing state of inner door
  insideOpen = false;

  // indication of running animations
  outsideBusy = false;
  insideBusy = false;
  pressureEqualizationBusy = false;
}

function startPressureEqualization(value, timestamp) {
  // isActive == true if button pressed
  if (value) {
    if (pressureEqualizationBusy) {
      // Pressure equalization not allowed
      if(pressureIndoors) {
        alarmAudio.startTime = timestamp;
      }
    }
    else {
      if(outsideOpen || insideOpen) {
        // NOT ALLOWED
        displayText.string = new MFString (
        'Achtung!',
        'Druckausgleich',
        'nicht möglich!');
        if(insideOpen) {
          alarmAudio.startTime = timestamp;
        }
      } else {
        if(outsideBusy == false && insideBusy == false) {
          pressureEqualizationBusy = true;
          if (pressureIndoors) {
            pressFallingClock.startTime = timestamp;
            displayText.string = new MFString (
            'Bitte warten.',
            'Druckausgleich läuft.');
            pressureAudio.startTime = timestamp;
          }
          else {
            pressRaiseClock.startTime = timestamp;
            displayText.string = new MFString (
            'Bitte warten.',
            'Druckausgleich läuft.');
            pressureAudio.startTime = timestamp;
          }
        } else {
          if(pressureIndoors) {
            alarmAudio.startTime = timestamp;
          }
        }
      }
    }
  }
}

function pressureIncreaseFinished (value, timestamp)
{
  // if pressureRaise_CLOCK finished
  if(value == false) {
    // Inside Pressure reached
    pressureIndoors = true;
    pressureEqualizationBusy = false;

    displayText.string = new MFString (
    'Druckausgleich fertig.',
    'Innere Tür entriegelt.');
  }
}

function pressureDecreaseFinished (value, timestamp)
{
  // if pressureFalling_CLOCK finished
  if(value == false) {
    // Outside Pressure reached
    pressureIndoors = false;
    pressureEqualizationBusy = false;

    displayText.string = new MFString (
    'Druckausgleich fertig.',
    'Äußere Tür entriegelt.');
  }
}

function leverOutsideClick (value, timestamp) {
  if(value && outsideBusy == false){
    if(outsideOpen) {
      doorOutsideClosingClock.startTime = timestamp;
      outsideBusy=true;
      displayText.string = new MFString (
      'Äußere Tür',
      'schließt sich.');
    } else {
      // outside closed --> open
      if(insideOpen) {
        // DANGER!!!
        // inside door open
        displayText.string = new MFString (
        'Achtung!',
        'Innere Tür noch',
        'offen.');
        alarmAudio.startTime = timestamp;
      } else {
        if(pressureIndoors || pressureEqualizationBusy) {
          // DANGER no equalized pressure
          displayText.string = new MFString (
          'Achtung!',
          'Druckausgleich fehlt.');
          alarmAudio.startTime = timestamp;
        } else {
          // ALRIGHT!!
          // Start opening animation

          displayText.string = new MFString (
          'Äußere Tür',
          'öffnet sich.');
          doorOutsideOpeningClock.startTime = timestamp;
          outsideBusy=true;
        }
      }
    }
  }
}

function leverInsideClick (value, timestamp) {
  if(value && insideBusy == false) {
    if(insideOpen) {
      insideDoorClosingClock.startTime = timestamp;
      insideBusy=true;
      displayText.string = new MFString (
      'Innere Tür',
      'schließt sich.');
      doorAudio.startTime = timestamp;
    } else {
      if(outsideOpen) {
        // DANGER!!!
        // outside door open
        displayText.string = new MFString (
        'Achtung!',
        'Äußere Tür noch ',
        'offen.');
      } else {
        if(pressureIndoors==false || pressureEqualizationBusy) {
          // DANGER no equalized pressure
          displayText.string = new MFString (
          'Achtung!',
          'Druckausgleich fehlt.');
          alarmAudio.startTime = timestamp;
        } else {
          // ALRIGHT!!
          // Start closing animation
          insideDoorOpeningClock.startTime = timestamp;
          insideBusy=true;
          displayText.string = new MFString (
          'Innere Tür',
          'öffnet sich.');
          leverAudio.startTime = timestamp;
        }
      }
    }
  }
}

function outsideDoorOpeningFinished(value, timestamp) {
  if(value == false) {
    // outside door is now open
    outsideOpen=true;
    outsideBusy=false;
    displayText.string = new MFString (
    'Äußere Tür geöffnet.',
    'Für den Druck-',
    'ausgleich bitte',
    'wieder schließen!');
  }
}

function outsideDoorClosingFinished(value, timestamp) {
  if(value == false) {
    // outside door is now closed
    outsideOpen=false;
    outsideBusy=false;
    displayText.string = new MFString (
    'Äußere Tür ',
    'geschlossen.',
    'Druckaus-',
    'gleich möglich.');
  }
}

function insideDoorOpeningFinished(value, timestamp) {
  if(value == false) {
    // inside door is now open
    insideOpen=true;
    insideBusy=false;
    displayText.string = new MFString (
    'Innere Tür geöffnet.',
    'Für den Druck-',
    'ausgleich bitte',
    'wieder schließen!');
  }
}

function insideDoorClosingFinished(value, timestamp) {
  if(value == false) {
    // inside door is now closed
    insideOpen=false;
    insideBusy=false;
    displayText.string = new MFString (
    'Innere Tür ge-',
    'schlossen.',
    'Druckaus-',
    'gleich möglich.');
  }
}
